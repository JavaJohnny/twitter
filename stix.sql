-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 13, 2018 at 03:05 PM
-- Server version: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `stix`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `body` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `post_id`, `body`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Test Comment Test Comment', '2018-08-13 09:25:10', '2018-08-13 09:25:10'),
(2, 1, 1, 'Hello There!', '2018-08-13 09:25:33', '2018-08-13 09:25:33'),
(3, 2, 2, 'His name was Sir Cumference', '2018-08-13 10:53:34', '2018-08-13 10:53:34'),
(4, 3, 2, 'I CAN\'T BREATH!', '2018-08-13 11:00:05', '2018-08-13 11:00:05'),
(5, 5, 4, 'Hello sugar momma!', '2018-08-13 11:11:31', '2018-08-13 11:11:31'),
(6, 6, 2, 'I\'M DEAD!', '2018-08-13 11:13:57', '2018-08-13 11:13:57'),
(7, 6, 4, 'holy shit! FAMOUS PEOPLE!!', '2018-08-13 11:14:55', '2018-08-13 11:14:55'),
(8, 1, 2, 'O_O', '2018-08-13 11:23:26', '2018-08-13 11:23:26'),
(9, 1, 4, 'HUBBA HUBBAA', '2018-08-13 11:29:33', '2018-08-13 11:29:33'),
(10, 7, 2, 'hAHAAH', '2018-08-13 20:51:03', '2018-08-13 20:51:03');

-- --------------------------------------------------------

--
-- Table structure for table `dislikes`
--

DROP TABLE IF EXISTS `dislikes`;
CREATE TABLE IF NOT EXISTS `dislikes` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `followers`
--

DROP TABLE IF EXISTS `followers`;
CREATE TABLE IF NOT EXISTS `followers` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `follower_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `followers_follower_id_foreign` (`follower_id`),
  KEY `followers_user_id_foreign` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `followers`
--

INSERT INTO `followers` (`id`, `user_id`, `follower_id`, `created_at`, `updated_at`) VALUES
(1, 2, 4, '2018-08-13 11:04:42', '2018-08-13 11:04:42'),
(2, 1, 4, '2018-08-13 11:04:45', '2018-08-13 11:04:45'),
(3, 3, 4, '2018-08-13 11:04:50', '2018-08-13 11:04:50'),
(4, 1, 6, '2018-08-13 11:14:16', '2018-08-13 11:14:16'),
(5, 2, 6, '2018-08-13 11:14:19', '2018-08-13 11:14:19'),
(6, 4, 6, '2018-08-13 11:14:22', '2018-08-13 11:14:22'),
(7, 5, 6, '2018-08-13 11:14:26', '2018-08-13 11:14:26'),
(8, 2, 1, '2018-08-13 11:28:06', '2018-08-13 11:28:06'),
(9, 3, 7, '2018-08-13 20:51:58', '2018-08-13 20:51:58'),
(10, 3, 8, '2018-08-13 20:54:14', '2018-08-13 20:54:14');

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

DROP TABLE IF EXISTS `likes`;
CREATE TABLE IF NOT EXISTS `likes` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=94 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(86, '2014_10_12_000000_create_users_table', 2),
(87, '2014_10_12_100000_create_password_resets_table', 2),
(88, '2018_07_26_180239_create_posts_table', 2),
(89, '2018_07_26_180301_create_comments_table', 2),
(90, '2018_08_01_144530_create_profiles_table', 2),
(68, '2018_08_02_173718_create_likes_table', 1),
(91, '2018_08_08_162915_create_followers_table', 2),
(92, '2018_08_09_151642_create_likes_table', 2),
(93, '2018_08_09_165209_create_dislikes_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `body`, `created_at`, `updated_at`) VALUES
(2, 2, 'Have you heard about the King Sperm, that was good at math?', '2018-08-13 10:52:27', '2018-08-13 10:53:14'),
(3, 3, 'LOL! @Jamie Lee Carter look at his post!', '2018-08-13 10:59:12', '2018-08-13 10:59:12'),
(4, 4, 'Hello boys~', '2018-08-13 11:07:52', '2018-08-13 11:07:52'),
(5, 5, 'DID YOU SMELL!!!!!!!!!!', '2018-08-13 11:10:37', '2018-08-13 11:10:37'),
(6, 5, 'WHAT THE ROCK IS COOKING !!!?!', '2018-08-13 11:10:52', '2018-08-13 11:17:50'),
(1, 1, 'I never said most of the things I said.', '2018-08-13 11:23:02', '2018-08-13 11:23:02'),
(8, 7, 'test post', '2018-08-13 20:48:39', '2018-08-13 20:48:39'),
(9, 8, 'ewrrwerwe', '2018-08-13 20:53:48', '2018-08-13 20:53:48'),
(10, 8, 'ewrewrwe', '2018-08-13 20:53:52', '2018-08-13 20:53:52');

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

DROP TABLE IF EXISTS `profiles`;
CREATE TABLE IF NOT EXISTS `profiles` (
  `user_id` int(11) NOT NULL,
  `bio` text COLLATE utf8mb4_unicode_ci,
  `gender` varchar(7) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthplace` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth_month` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth_day` int(3) DEFAULT NULL,
  `birth_year` year(4) DEFAULT NULL,
  `phone_number` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `education` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`user_id`, `bio`, `gender`, `birthplace`, `birth_month`, `birth_day`, `birth_year`, `phone_number`, `location`, `education`, `created_at`, `updated_at`) VALUES
(1, '- Creator -\r\n- Artist -\r\n\r\n\r\nBelieve you can and you\'re halfway there. -\r\nTheodore Roosevelt', NULL, 'Manila', 'January', 1, 2000, '(587) - 968-XX49', 'Calgary, AB', 'Innotech College', '2018-08-13 08:25:40', '2018-08-13 09:05:49'),
(2, 'When human beings experience trauma or severe life stressors, it is not uncommon for their lives to unravel. My great passion is bringing healing to people who have been through a traumat', NULL, 'Hong Kong, NY', 'March', NULL, 1994, NULL, NULL, NULL, '2018-08-13 10:38:01', '2018-08-13 10:51:58'),
(3, 'fter moving to Washington, D.C. (because she was burnt out and didn’t know where else to go) and working briefly as a project manager for The Smithsonian Associates', NULL, 'Toronto,AB', 'April', NULL, 1995, NULL, NULL, NULL, '2018-08-13 10:55:54', '2018-08-13 10:58:26'),
(4, 'Hello Im Famous!', NULL, 'Portland,WA', 'March', 19, 1992, '(587) - 968-XX49', 'Portland,WA', 'Innotech College', '2018-08-13 11:01:04', '2018-08-13 11:06:40'),
(5, 'Add Your Bio', NULL, 'Edmonton,CA', 'February', 14, 1984, '(403)-280-XX46', 'Los Angeles, BC', 'Father Scollen', '2018-08-13 11:09:15', '2018-08-13 11:10:21'),
(6, 'I specialize in logo design, branding, web design and offer design services to businesses of all sizes around the world, ultimately improving their bottom line by crafting creative soluti', NULL, 'Yo Mom\'s House', 'February', 17, 1986, '(587) - 968-XX49', 'Portland,WA', 'Innotech College', '2018-08-13 11:12:25', '2018-08-13 11:13:39'),
(7, 'this is my bio, my project is so cool im so proud of it , use css grid!', NULL, 'Yo Mom\'s House', 'January', 1, 2000, '(587) - 968-XX49', 'Toronto,AB', 'Innotech College', '2018-08-13 20:47:01', '2018-08-13 20:50:06'),
(8, 'Add Your Bio', NULL, 'Manila', 'April', NULL, 1994, NULL, NULL, NULL, '2018-08-13 20:53:39', '2018-08-13 20:53:39');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default.png',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `avatar`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'John J Medina', 'me@email.com', '$2y$10$N1yTk89bETLCdSbz9Mwh9.C6/jpCJMWIQ.SXrmULSTwU9gKNj6QfS', '1534129549.jpg', 'an3gdOUODK81wfpiG2crd20qadl1ygGzde5hD6i7WPA5fsLmJnzXhKabMngs', '2018-08-13 08:25:40', '2018-08-13 09:05:49'),
(2, 'Jamie Lee Carter', 'jlc@email.com', '$2y$10$cxJfdX2qK7k3RdGfmZGyx.6A22JO8VMT2COpVDQ8qfrmRxWc6tVNq', '1534135918.jpg', 'nRiTy8HRzoovWMAJeV71C7yP6AjPlErMmIsu7FMfGvdgsCaOHV6iXs2VleQE', '2018-08-13 10:38:01', '2018-08-13 10:51:58'),
(3, 'Michael Michael', 'mlj@email.com', '$2y$10$WvyiF8SNXHuX71wq0cJo7.ADrpCFm2kMI0Yr./YsvVvodmOPJSFvq', '1534136306.jpg', 'zGbHy4kACt7TPHdNaBPtdRDnPmw1AEUfUaOy9kxbigbKgEqHH45gghpzAoWq', '2018-08-13 10:55:54', '2018-08-13 10:58:26'),
(4, 'Angelina Jolie', 'al@email.com', '$2y$10$4bmr12G2Ij578/omS7No7uD8RY5Z2.Ikl0dpiG17BHtlBiH1E27FO', '1534136673.jpg', '3DA5NQ0XI0IYbuPQSpGkCeR4OuDJ3jwR2mGFuoCZpSbxNn3t0pPojeZ50P2L', '2018-08-13 11:01:04', '2018-08-13 11:04:33'),
(5, 'The Rock', 'tr@email.com', '$2y$10$nLo9I6MGwQtfdHMZCmYjY.FTF2H.W570MYmEJ4AZ/328W0cXPaf16', '1534137021.jpg', 'nij8Y53GdpLXcOj9Zu6grau1uVU8u3s2iV7FJgumDqhT2gb1bzQhN4Uv2nNZ', '2018-08-13 11:09:15', '2018-08-13 11:10:21'),
(6, 'Lisa Baldwin', 'lb@email.com', '$2y$10$hMYa94oFxZrJPqxU949yEO82F7eNaUbKGAjDzDQL.4IZQACWRyH4K', '1534137219.jpg', 'v953MENh3rN1eBPQi68oqXx898Tl63oC39DlcbqI0zKLcqCJZwsbyzKCfT7o', '2018-08-13 11:12:25', '2018-08-13 11:13:39'),
(7, 'Test Account', 'ta@email.com', '$2y$10$kyJOrAaDiIRnPu.SOiqzIOWgSeO7P/ydKOyxI5KbcornijwsHg/DS', '1534171806.jpg', NULL, '2018-08-13 20:47:01', '2018-08-13 20:50:06'),
(8, 'John J Medina', 'me@email.com', '$2y$10$96gZB0g1Zr5OcQfK8kq1GuLxT3lXvb5ytg8CW0M7zulhO6s.JZ36i', 'default.png', NULL, '2018-08-13 20:53:39', '2018-08-13 20:53:39');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
