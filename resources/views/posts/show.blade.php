@extends('layouts.master')

@section('content')

<div class="main">

</div>

<div class="main" id="show">
    <div class="tweet-show">
        <div class="tweet-avatar">
            <div class="tweetlogo">
                <img src="/uploads/avatars/{{  $posts->user->avatar }}" alt="avatar">
            </div>
        </div>
        <div class="tweet-name">
            <h2>{{$posts->user->name}}</h2>
        </div>
        <div class="tweet-body">
            <div class="tweet-bubble">
                <p>
                    {{ $posts->body }}
                </p>
            </div>
        </div>
        <div class="tweet-interaction">
            <a href="/post/{post}/like" class="like"><i class="fa fa-thumbs-up"></i></a>
            <a href="/post/{post}/dislike" class="like"><i class="fa fa-thumbs-down"></i></a>
            @if(Auth::id() == $posts->user_id)
            <a href="/post/{{ $posts->id }}/edit">Edit</a>
            @endif
        </div>
        <div class="tweet-datetime">
            <em>
                {{ $posts->created_at->diffForHumans() }}
            </em>
        </div>
    </div>

    <div class="comments">
        <ul class="commentline">
            @foreach($comments as $comment)
            <li>
                <div class="comment-avatar">
                    <a href="/profile/{{ $comment->user_id }}"><img src="/uploads/avatars/{{  $comment->user->avatar }}" ></a>
                </div>
                <div class="commentBody">
                    <div class="commentBubble">
                        <h3>{{$comment->user->name}}</h3>
                        <br/>
                        <p>{{$comment->body}}</p>
                    </div>
                    <div class="datetime">
                        <em> {{$comment->created_at->diffForHumans()}} </em>
                        @if(Auth::id() == $comment->user_id)
                        <a href="/post/{{ $comment->id }}/comments/edit">Edit</a>
                        @endif
                    </div>
                </div>
            </li>
            @endforeach
        </ul>
        <div class="commentForm">
            <form method="POST" action="/post/{{$posts->id}}/comments">
                {{ csrf_field() }}
                <div class="form-group">
                    <textarea name="body" placeholder="Your Comment here." class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Comment</button>
                </div>
            </form>
        </div>
    </div>


</div>

@endsection
