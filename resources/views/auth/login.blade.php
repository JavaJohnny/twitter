<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/login.css">
    <link rel="stylesheet" href="/css/fonts/font.css">
    <title>Login</title>
</head>

<body>

    <div class="grid">
        <div class="main">
            <div class="content">
                <div class="title">
                    <h1>Stix</h1>
                </div>
                <div class="message">
                    <p>
                        Connect with your friends and other fascinating people.
                        Get in-the-moment updates on the things that interest you.
                    </p>
                </div>
                <div class="copyright">
                    <p>Copyright &copy; JJM 2018</p>
                </div>
            </div>
        </div>
        <div class="login">
            <form method="POST" action="/login">
                {{ csrf_field() }}
                <div class="title">
                    <h2> Login </h2>
                </div>
                <div class="form-group">
                    <input type="email" name="email" placeholder="Enter your Email Adress" required autofocus>
                </div>
                <div class="form-group">
                    <input type="password" name="password" placeholder="Enter your Password" required>
                </div>
                <button type="submit">Login</button>
            </form>
        </div>
        <div class="register">
                <div class="title">
                    <h2>Be a Member</h2>
                </div>
                    <a href="/register">Register</a>
        </div>
    </div>

</body>

</html>
