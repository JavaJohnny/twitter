<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <link rel="stylesheet" href="/css/profile.css">
    <link rel="stylesheet" href="/css/fonts/font.css">
    <title>Profile</title>
</head>

<body>
    <div class="grid">

        <div class="head">
            <div class="logo">
                <img src="/css/img/stix-logo.png" alt="logo">
            </div>
            <div class="nav">
                <h2><a href="/">Home<span class="faicon"><i class="fa fa-home"></span></i></a></h2>
                <h2><a href="/profile/{{{ Auth::user()->id }}}">Profile<span class="faicon"><i class="fa fa-user"></span></i></a></h2>
                <h2><a href="/logout">Logout<span class="faicon"><i class="fa fa-power-off"></span></i></a></h2>
            </div>
        </div>
        <div class="aside">
            <div class="avatar-card">
                <div class="profile-avatar">
                    <img src="/uploads/avatars/{{  $user->avatar }}" alt="avatar">
                </div>
                <div class="profile-name">
                    <h2>{{$user->name}}</h2>
                </div>
            </div>
        </div>

        <div class="main">
            <div class="profile-card">

                <ul class="profile-info">
                    <li>
                        <h4>
                            Gender:
                        </h4>
                        <p>
                            Male
                        </p>
                    </li>
                    <li>
                        <h4>
                            Birthday:
                        </h4>
                        <p>
                            {{$user->profile->birth_month}} {{$user->profile->birth_day}}, {{$user->profile->birth_year}}
                        </p>
                    </li>
                    <li>
                        <h4>
                            Birthplace:
                        </h4>
                        <p>
                            {{$user->profile->birthplace}}
                        </p>
                    </li>
                    <li>
                        <h4>
                            Phone Number:
                        </h4>
                        <p>
                            {{$user->profile->phone_number}}
                        </p>
                    </li>
                    <li>
                        <h4>
                            Education:
                        </h4>
                        <p>
                            {{$user->profile->education}}
                        </p>
                    </li>
                    <li>
                        <h4>
                            Bio:
                        </h4>
                        <p>
                            {{ $user->profile->bio }}
                        </p>
                    </li>
                </ul>
                <div class="profileInteract">
                    <a href="{{ route('user.follow', $user->id) }}" class="btn">Follow User</a>
                    <a href="{{ route('user.unfollow', $user->id) }}" class="btn">Unollow User</a>
                    <a href="/profile/{{ $user->id }}/edit" class="btn">Edit Profile</a>
                </div>
            </div>
            <div class="counter">
                <div class="counter-card">
                    <div class="counter-card-item">
                        <h3>
                            Followers:
                        </h3>
                        <p>
                            25
                        </p>
                    </div>
                </div>
                <div class="counter-card">
                    <div class="counter-card-item">
                        <h3>
                            Post:
                        </h3>
                        <p>
                            25
                        </p>
                    </div>
                </div>
                <div class="counter-card">

                    <div class="counter-card-item">
                        <h3>
                            Following:
                        </h3>
                        <p>
                            20
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer">
            <p>
                Copyright &copy; JJM 2018
            </p>
        </div>
    </div>
</body>

</html>
