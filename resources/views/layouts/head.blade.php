<div class="logo">
    <img src="/css/img/stix-logo.png" alt="logo">
</div>
<div class="nav">
    <h2><a href="/">Home<span class="faicon"><i class="fa fa-home"></span></i></a></h2>
    <h2><a href="/profile/{{ Auth::user()->id }}">Profile<span class="faicon"><i class="fa fa-user"></span></i></a></h2>
    <h2><a href="/logout">Logout<span class="faicon"><i class="fa fa-power-off"></span></i></a></h2>
</div>
