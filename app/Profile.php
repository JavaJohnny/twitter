<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Profile extends Model
{

    protected $primaryKey = 'user_id';

    protected $fillable = [
        'user_id', 'bio', 'birthplace', 'birth_month', 'birth_year', 'phone_number', 'education', 'location'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function publish(User $user)
    {
        $this->users()->save();
    }
}
