<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Comment;
use App\User;

class Post extends Model
{
    protected  $fillable = ['user_id', 'title', 'body'];

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function addComment($data)
    {
        $this->comments()->create($data);
    }

    public function likes()
    {
        return $this->belongsToMany(User::class, 'likes', 'user_id', 'post_id');
    }

    public function dislikes()
    {
        return $this->belongsToMany(User::class, 'dislikes', 'user_id', 'post_id');
    }
}
