<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Profile;
use Illuminate\Support\Facades\Hash;


class RegistrationController extends Controller
{
    public function create()
    {

        return view('auth.register');
    }

    public function store(Request $request)
    {
        // Validate the form
        $data = $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $data['password'] = bcrypt($data['password']);

        // Create and save the User
        $user = User::create($data);
        $profile = Profile::create([
            'user_id' => $user->id,
            'bio' => 'Add Your Bio',
            'gender' => $request->input('gender'),
            'birth_month' => $request->input('birth_month'),
            'birth_day' => $request->input('birth_day'),
            'birth_year' => $request->input('birth_year'),
            'birthplace' => $request->input('birthplace')

        ]);
        // Sign them In
        auth()->login($user);
        // Redirect to main page
        return redirect('/');


    }

}
